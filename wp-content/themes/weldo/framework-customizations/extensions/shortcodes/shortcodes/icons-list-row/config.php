<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Icons List in Row', 'weldo' ),
		'description' => esc_html__( 'Several Icons Lists in row', 'weldo' ),
		'tab'         => esc_html__( 'Content Elements', 'weldo' ),
	)
);