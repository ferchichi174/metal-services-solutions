<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Progress Bar', 'weldo' ),
	'description' => esc_html__( 'Add a Bootsrap progress bar', 'weldo' ),
	'tab'         => esc_html__( 'Content Elements', 'weldo' )
);