<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Info box', 'weldo' ),
		'description' => esc_html__( 'Display block with icon and features', 'weldo' ),
		'tab'         => esc_html__( 'Content Elements', 'weldo' )
	)
);