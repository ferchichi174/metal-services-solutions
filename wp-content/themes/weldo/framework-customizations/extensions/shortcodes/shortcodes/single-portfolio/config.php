<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Single Portfolio', 'weldo' ),
		'description' => esc_html__( 'Single Portfolio', 'weldo' ),
		'tab'         => esc_html__( 'Widgets', 'weldo' )
	)
);