<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Tabs with Accordion', 'weldo' ),
		'description' => esc_html__( 'Display collapse accordion in tabs', 'weldo' ),
		'tab'         => esc_html__( 'Content Elements', 'weldo' )
	)
);