<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Custom Container End', 'weldo' ),
	'description' => esc_html__( 'Wrap elements in custom container. Closing container tag', 'weldo' ),
	'tab'         => esc_html__( 'Layout Elements', 'weldo' )
);