<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Social Icons', 'weldo' ),
	'description' => esc_html__( 'Add set of social icons', 'weldo' ),
	'tab'         => esc_html__( 'Content Elements', 'weldo' )
);