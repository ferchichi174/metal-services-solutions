<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Media carousel', 'weldo' ),
		'description' => esc_html__( 'Show images carousel with optional links', 'weldo' ),
		'tab'         => esc_html__( 'Media Elements', 'weldo' ),
	)
);