<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => esc_html__('Image', 'weldo'),
	'description'   => esc_html__('Add an Image', 'weldo'),
	'tab'           => esc_html__('Media Elements', 'weldo'),
);