<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}


$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Pie Chart', 'weldo' ),
	'description' => esc_html__( 'Add a Pie Chart', 'weldo' ),
	'tab'         => esc_html__( 'Content Elements', 'weldo' )
);