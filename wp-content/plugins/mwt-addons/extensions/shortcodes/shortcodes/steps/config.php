<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Steps', 'weldo' ),
	'description' => esc_html__( 'Add a steps', 'weldo' ),
	'tab'         => esc_html__( 'Content Elements', 'weldo' )
);