<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Icons in list', 'weldo' ),
		'description' => esc_html__( 'Several icons in bordered list', 'weldo' ),
		'tab'         => esc_html__( 'Content Elements', 'weldo' ),
	)
);