<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Post Tabs', 'weldo' ),
		'description' => esc_html__( 'Post thumbnails in tabs', 'weldo' ),
		'tab'         => esc_html__( 'Widgets', 'weldo' )
	)
);