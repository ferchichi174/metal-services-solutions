<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Custom Container Start', 'weldo' ),
	'description' => esc_html__( 'Wrap elements in custom container. Opening container tag', 'weldo' ),
	'tab'         => esc_html__( 'Layout Elements', 'weldo' )
);