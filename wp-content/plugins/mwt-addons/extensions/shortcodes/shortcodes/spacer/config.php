<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Spacer', 'weldo' ),
	'description' => esc_html__( 'Add a responsive spacer', 'weldo' ),
	'tab'         => esc_html__( 'Content Elements', 'weldo' )
);