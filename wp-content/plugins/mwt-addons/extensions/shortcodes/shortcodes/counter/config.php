<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Counter', 'weldo' ),
		'description' => esc_html__( 'Counter with icon, title and description', 'weldo' ),
		'tab'         => esc_html__( 'Content Elements', 'weldo' ),
	)
);